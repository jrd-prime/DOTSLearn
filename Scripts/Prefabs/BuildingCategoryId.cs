﻿namespace Jrd.GameStates.BuildingState.Prefabs
{
    public enum BuildingCategoryId
    {
        Default,
        Factory,
        Field,
    }
}