﻿using Unity.Entities;

namespace Jrd.Select
{

    /// <summary>
    /// Selectable entity
    /// </summary>
    public struct SelectableTag : IComponentData
    {
    }
}