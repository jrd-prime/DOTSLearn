﻿using Unity.Entities;

namespace Jrd.UserInput.Components
{
    public struct ZoomDirectionData : IComponentData
    {
        public float ZoomDirection;
    }
}