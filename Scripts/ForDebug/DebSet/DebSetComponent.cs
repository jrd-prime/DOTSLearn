﻿using Unity.Entities;

namespace Jrd.ForDebug.DebSet
{
    public struct DebSetComponent : IComponentData
    {
        public bool MouseRaycast;
    }
}