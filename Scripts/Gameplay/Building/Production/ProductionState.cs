﻿namespace Jrd.Gameplay.Building.Production
{
    public enum ProductionState
    {
        Init,
        NotEnoughProducts,
        EnoughProducts,
        Started,
        InProgress,
        Stopped,
        Finished,
    }
}