﻿namespace Jrd.Gameplay.Building
{
    public enum BuildingNameId
    {
        Default,
        Windmill,
        Bakery,
        Warehouse,
        Seedbed
    }
}