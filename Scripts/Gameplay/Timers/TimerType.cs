﻿namespace Jrd.Gameplay.Timers
{
    public enum TimerType
    {
        MoveToWarehouse,
        OneLoadCycle,
        FullLoadCycle
    }
}