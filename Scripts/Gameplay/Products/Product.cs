﻿namespace Jrd.Gameplay.Products
{
    public enum Product
    {
        Wheat,
        Flour,
        Hay,
        Wood,
        WoodenPlank,
        Brick
    }
}