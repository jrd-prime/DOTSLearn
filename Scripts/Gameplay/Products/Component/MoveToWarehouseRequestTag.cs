﻿using Jrd.Gameplay.Storage.Warehouse;
using Unity.Entities;

namespace Jrd.Gameplay.Products.Component
{
    /// <summary>
    /// Move products to warehouse
    /// <see cref="MoveToWarehouseRequestSystem"/>
    /// </summary>
    public struct MoveToWarehouseRequestTag : IComponentData
    {
    }
}